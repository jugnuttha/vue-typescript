import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';

Vue.config.productionTip = false;

// https://alligator.io/vuejs/typescript-class-components/
// https://github.com/kaorun343/vue-property-decorator

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
